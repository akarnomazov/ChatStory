﻿using UnityEngine.SceneManagement;

class SceneLoader
{
    public void LoadScene(string sceneName) =>
        SceneManager.LoadScene(sceneName);
}
