﻿using UnityEngine;
using Zenject;

class ProjectControl
{
    public string currentStoryId { get; private set; }

    public ProjectControl(ProjectIntaller projectInstaller, SignalBus signalBus, SceneLoader sceneLoader)
    {
        Debug.Log(signalBus == null);
        signalBus.Subscribe<SelectStorySignal>(x =>
        {
            currentStoryId = x.storyId;
            projectInstaller.BindDialogueConfig(currentStoryId);
            sceneLoader.LoadScene("DialogueScene");
        });
    }
}
