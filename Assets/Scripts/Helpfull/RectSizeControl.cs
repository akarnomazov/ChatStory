using UnityEngine;

public class RectSizeControl
{
    public void IncreaseVertical(RectTransform content, float value) =>
        content.sizeDelta += new Vector2(0, value);

    public void ReduceVertical(RectTransform content, float value) =>
        content.sizeDelta -= new Vector2(0, value);
}
