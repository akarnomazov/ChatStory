﻿using UnityEngine;

public class MoveToPoint : MonoBehaviour
{
    #region Variables
    private RectTransform rect;
    private iTween.EaseType easetype = iTween.EaseType.easeInOutSine;
    private iTween.LoopType loop = iTween.LoopType.loop;
    private float moveTime, speed;
    public float MoveTime => moveTime;
    public void SetMoveTime(float moveTime) => this.moveTime = moveTime;
    public void SetSpeed(float speed) => this.speed = speed;
    #endregion

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
    }

    public void Move(Vector2 pointPos)
    {
        if (speed != 0) moveTime = Vector2.Distance(rect.anchoredPosition, pointPos) / speed;

        iTween.ValueTo(gameObject, iTween.Hash(
            "from", rect.anchoredPosition,
            "to", pointPos,
            "time", moveTime,
            "easetype", easetype,
            "onupdate", "SetPosition"));
    }

    public void MoveAdd(Vector2 addPos)
    {
        var targetPos = rect.anchoredPosition + addPos;
        if (speed != 0) moveTime = Vector2.Distance(rect.anchoredPosition, targetPos) / speed;

        iTween.ValueTo(gameObject, iTween.Hash(
            "from", rect.anchoredPosition,
            "to", targetPos,
            "time", moveTime,
            "easetype", easetype,
            "onupdate", "SetPosition"));
    }

    public void LoopMove(Vector2 startPos, Vector2 finalPos)
    {
        if (speed != 0) moveTime = Vector2.Distance(startPos, finalPos) / speed;

        iTween.ValueTo(gameObject, iTween.Hash(
            "from", startPos,
            "to", finalPos,
            "time", moveTime,
            "easetype", easetype,
            "looptype", loop,
            "onupdate", "SetPosition")) ;
    }

    private void SetPosition(Vector2 pos)
    {
        rect.anchoredPosition = pos;
    }
}
