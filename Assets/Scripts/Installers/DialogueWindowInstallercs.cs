using UnityEngine;
using Zenject;

public class DialogueWindowInstallercs : MonoInstaller
{
    [SerializeField] private DialogueView dialogueWindowView;
    [SerializeField] private MessageFactory messageFactory;
    [SerializeField] private Transform messageParent;
    [SerializeField] private GameObject defaulMessagePref, mainActorMessagePref, neutralMessagePref;

    public override void InstallBindings()
    {
        Container
            .Bind<MessageFactory>()
            .FromInstance(messageFactory)
            .AsSingle();

        Container
            .Bind<DialogueView>()
            .FromInstance(dialogueWindowView)
            .AsSingle();

        Container
            .Bind<DialogueControl>()
            .AsSingle().NonLazy();

        Container
            .Bind<DialogueModel>()
            .AsSingle();
    }
}