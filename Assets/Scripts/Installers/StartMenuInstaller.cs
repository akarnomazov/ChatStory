using UnityEngine;
using Zenject;

namespace StartMenu
{
    public class StartMenuInstaller : MonoInstaller
    {
        [SerializeField] private StartMenuView startMenuView;
        [SerializeField] private SelectorElementsFactory elemsFactory;
        [SerializeField] private Transform selectorElemsParent;
        private GameObject selectorElemPref; 

        public override void InstallBindings()
        {
            BindSelectorElementFactory();
            BindStartMenuMVC();
        }

        private void BindStartMenuMVC()
        {
            Container
                .Bind<StartMenuView>()
                .FromInstance(startMenuView)
                .AsSingle();

            Container
                .Bind<StartMenuModel>()
                .AsSingle()
                .NonLazy();

            Container
                .Bind<StartMenuControl>()
                .AsSingle()
                .NonLazy();
        }

        private void BindSelectorElementFactory()
        {
            selectorElemPref = Resources.Load<GameObject>("StartMenuPrefs/StoryBtnPref");

            Container.BindFactory<SelectorElement, SelectorElement.Factory>()
                .FromComponentInNewPrefab(selectorElemPref).OnInstantiated(SelectorElementInstantiateCallback);

            Container
                .Bind<SelectorElementsFactory>()
                .FromInstance(elemsFactory)
                .AsSingle();
        }

        private void SelectorElementInstantiateCallback(InjectContext arg1, object arg2)
        {
            ((MonoBehaviour)arg2).transform.SetParent(selectorElemsParent);
        }
    }
}