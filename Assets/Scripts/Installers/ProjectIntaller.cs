using Zenject;
using UnityEngine;
using System.Linq;

public class ProjectIntaller : MonoInstaller
{
    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);
        Container.DeclareSignal<SelectStorySignal>();

        Container
            .Bind<ProjectIntaller>()
            .FromInstance(this)
            .AsSingle();

        Container
            .Bind<SceneLoader>()
            .AsSingle()
            .NonLazy();

        Container
            .Bind<ProjectControl>()
            .AsSingle()
            .NonLazy();

    }

    public void BindDialogueConfig(string storyId)
    {
        var dialogueConfig = Resources
            .LoadAll<DialogueContainer>("ProductionStories")
            .First(config => config.StoryId == storyId);

        if (dialogueConfig == null) return;

        Container
            .Bind<DialogueContainer>()
            .FromScriptableObject(dialogueConfig)
            .AsSingle();
    }
}