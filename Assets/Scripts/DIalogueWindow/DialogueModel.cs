﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using Zenject;
using System;

public enum MessageType { Default, MainActor, Neutral}

public class ChoiceInfo
{
    public string Guid { get; }
    public string Text { get; }

    public ChoiceInfo(string guid, string text)
    {
        Guid = guid;
        Text = text;
    }
}

public class DialogueModel
{
    private enum WindowState { SelectChoice, Default}

    private DialogueContainer someConfig;
    private string lastMessageGUID;
    private DialogueView view;
    private WindowState state = WindowState.Default;
    private Vector3 mainActorColorHSV;
    private ChoiceInfo[] choiceGuids = new ChoiceInfo[3];

    [Inject]
    public DialogueModel(DialogueContainer dialogueConfig, DialogueView view)
    {
        this.view = view;
        someConfig = dialogueConfig;

        Color.RGBToHSV(
            someConfig.ExposedProperties[0].ActorColor,
            out mainActorColorHSV.x, 
            out mainActorColorHSV.y,
            out mainActorColorHSV.z);
    }

    public void SelectStory(string storyName)
    {
        someConfig = Resources.Load<DialogueContainer>(storyName);
    }

    public void ClickHandler()
    {
        switch (state)
        {
            case WindowState.SelectChoice:
                return;
            case WindowState.Default:
                BuildMessageData();
                break;
        }
    }

    public void SelectChoiceHandler(string guid)
    {
        var messageData = someConfig.DialogueNodeData.Find(node => node.Guid == guid);
        view.HideChoices();
        state = WindowState.Default;
        DisplayMessage(messageData, MessageType.MainActor, mainActorColorHSV);
    }

    private void BuildMessageData()
    {
        DialogueNodeData messageData = null;
        bool mainActor = false;
        Vector3 messageColorHSV;
        if (string.IsNullOrEmpty(lastMessageGUID))
            messageData = someConfig.DialogueNodeData.First(x => x.Guid == someConfig.NodeLinks[0].TargetNodeGuid);
        else
        {
            List<NodeLinkData> links = someConfig.NodeLinks.FindAll(x => x.BaseNodeGuid == lastMessageGUID);
            if (links.Count == 0)
            {
                Debug.Log($"Dialogue end");
                return;
            }
            else if (links.Count > 1)
            {
                state = WindowState.SelectChoice;
                GenerateChoices(links);
                return;
            }
            else
                messageData = someConfig.DialogueNodeData.First(x => x.Guid == links[0].TargetNodeGuid);
        }
        
        var type = MessageType.Default;
        
        Debug.Log($"property actor {someConfig.ExposedProperties[9].ActorName }, message actor {messageData.ActorName}, {someConfig.ExposedProperties[9].ActorName == messageData.ActorName}");
        if (someConfig.ExposedProperties[0].ActorName == messageData.ActorName)
            type = MessageType.MainActor;
        else if (someConfig.ExposedProperties[9].ActorName == messageData.ActorName)
            type = MessageType.Neutral;
        if (mainActor)
            messageColorHSV = mainActorColorHSV;
        else Color.RGBToHSV(
            someConfig.ExposedProperties.First(actor => actor.ActorName == messageData.ActorName).ActorColor,
            out messageColorHSV.x, out messageColorHSV.y, out messageColorHSV.z);
        DisplayMessage(messageData, type, messageColorHSV);
    }

    private void DisplayMessage(DialogueNodeData messageData, MessageType type, Vector3 colorHSV)
    {
        lastMessageGUID = messageData.Guid;
        view.CreateMessage(messageData, type, colorHSV);
    }

    private void GenerateChoices(List<NodeLinkData> links)
    {
        for(int i = 0; i < links.Count; i++)
        {
            var nodeDataTmp = someConfig.DialogueNodeData.Find(node => node.Guid == links[i].TargetNodeGuid);
            if (nodeDataTmp != null)
                choiceGuids[i] = new ChoiceInfo(nodeDataTmp.Guid, links[i].PortName);
            else 
                choiceGuids[i] = null;
        }

        view.CreateChoices(choiceGuids);        
        view.ShowChoices();
    }
}
