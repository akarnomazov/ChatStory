using System;
using UnityEngine;

public class MessageFactory : MonoBehaviour
{
    [SerializeField] private Transform messageParent, choiceParent;
    [SerializeField] private GameObject defaultMessagePref, mainActorMessagePref, neutralMessagePref, choicePref;

    private void Awake()
    {
        //defaultMessagePref = Resources.Load<GameObject>("Prefabs/DefaultMessagePrefab");
        //mainActorMessagePref = Resources.Load<GameObject>("Prefabs/MainActorMessagePrefab");
        //neutralMessagePref = Resources.Load<GameObject>("Prefabs/NeutralMessagePrefab");
        //choicePref = Resources.Load<GameObject>("Prefabs/ChoicePrefab"); 
    }

    public DialogueMessage GetMessage(MessageType type)
    {
        DialogueMessage message = null;
        switch (type)
        {
            case MessageType.Default:
                message = Instantiate(defaultMessagePref, messageParent).GetComponent<DialogueMessage>();
                break;
            case MessageType.MainActor:
                message = Instantiate(mainActorMessagePref, messageParent).GetComponent<DialogueMessage>();
                break;
            case MessageType.Neutral:
                message = Instantiate(neutralMessagePref, messageParent).GetComponent<DialogueMessage>();
                break;
            default:
                break;
        }
        
        return message;
    }

    public SelectChoiceBtn GetChoiceBtn()
    {
        var instance = Instantiate(choicePref, choiceParent);
        return instance.GetComponent<SelectChoiceBtn>();
    }
}
