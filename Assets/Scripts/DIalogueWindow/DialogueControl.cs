﻿using Zenject;

public class DialogueControl
{
    [Inject]
    public DialogueControl(DialogueModel model, DialogueView view)
    {
        view.ClickEvent += () => model.ClickHandler();
        view.SelectChoiceEvent += guid => model.SelectChoiceHandler(guid);
    }
}