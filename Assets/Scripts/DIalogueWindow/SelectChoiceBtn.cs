﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SelectChoiceBtn : MonoBehaviour
{
    [SerializeField] private Button selectBtn;
    [SerializeField] private TMP_Text choiceText;

    public Button GetBtn() => selectBtn;
    public void SetChoiceText(string text) => choiceText.text = text;
}
