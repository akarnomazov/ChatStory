﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class DialogueView : MonoBehaviour
{
    [SerializeField] private GameObject choiceSelector;
    [SerializeField] private RectTransform messagePanelRect, choicesPanelRect;
    [SerializeField] private MoveToPoint dialoguePanelMover;
    [SerializeField] private GameObject choicesBlockScreen;
    private readonly float dialoguePanelMoveTime = 0.5f;
    private MessageFactory messageFactory;
    private List<DialogueMessage> messages = new List<DialogueMessage>();

    public Action ClickEvent;
    public Action<string> SelectChoiceEvent;

    [Inject]
    private void Construct(MessageFactory messageFactory) => this.messageFactory = messageFactory;

    private void Awake()
    {
        dialoguePanelMover.SetMoveTime(dialoguePanelMoveTime);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            ClickEvent?.Invoke();
    }

    public void ShowChoices()
    {
        dialoguePanelMover.MoveAdd(new Vector2(0f, 300f));
        choicesBlockScreen.SetActive(false);
    }

    public void HideChoices()
    {
        choicesBlockScreen.SetActive(true);
        dialoguePanelMover.MoveAdd(new Vector2(0f, -300f));
        Invoke("ClearSelector", dialoguePanelMoveTime);
    }

    private void ClearSelector()
    {
        foreach (Transform choice in choiceSelector.transform)
            Destroy(choice.gameObject);
    }

    public void CreateMessage(DialogueNodeData messageData, MessageType type, Vector3 colorHSV)
    {
        var instance = messageFactory.GetMessage(type);
        instance.GetComponent<DialogueMessage>()
            .Initialize(messageData.DialogueText, colorHSV, messageData.ActorName);
        messages.Add(instance);
        Invoke("UpdateMessagePanel", Time.deltaTime);
    }

    //Invoked method
    private void UpdateMessagePanel() =>
        LayoutRebuilder.ForceRebuildLayoutImmediate(messagePanelRect);

    //Invoked method
    private void UpdateChoicesPanel() =>
        LayoutRebuilder.ForceRebuildLayoutImmediate(choicesPanelRect);

    public void CreateChoices(ChoiceInfo[] choicesData)
    {
        for (int i = 0; i < choicesData.Length; i++)
        {
            if (choicesData[i] == null) continue;

            var tmpIndex = i;
            var choiceBtn = messageFactory.GetChoiceBtn();
            choiceBtn.SetChoiceText(choicesData[i].Text);
            choiceBtn.GetBtn().onClick.AddListener(() => SelectChoiceEvent.Invoke(choicesData[tmpIndex].Guid));
        }
        Invoke("UpdateChoicesPanel", Time.deltaTime);
    }
}