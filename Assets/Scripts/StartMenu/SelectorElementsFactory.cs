﻿using UnityEngine;
using Zenject;

namespace StartMenu
{
    public class SelectorElementsFactory : MonoBehaviour
    {
        [SerializeField] private Transform elementsParent;
        private GameObject elementPref;
        private SelectorElement.Factory factory;

        [Inject]
        private void Construct(SelectorElement.Factory factory)
        {
            this.factory = factory;
        }

        public SelectorElement GetElementInstance()
        {
            var instance = factory.Create();
            return instance.GetComponent<SelectorElement>();
        }
    }
}