using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace StartMenu
{
    public class SelectorElement : MonoBehaviour
    {
        [SerializeField] private Button startStoryBtn;
        [SerializeField] private TMP_Text title;
        private SignalBus signalBus;

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        public void Initialize(string storyId, string titleText)
        {
            title.text = titleText;

            startStoryBtn.onClick.AddListener(() =>
            {
                signalBus.Fire(new SelectStorySignal { storyId = storyId });
            });
        }

        public class Factory : PlaceholderFactory<SelectorElement> { }
    }
}
