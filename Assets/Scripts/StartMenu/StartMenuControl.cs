﻿using Zenject;

namespace StartMenu
{
    class StartMenuControl
    {
        public StartMenuControl(SignalBus signalBus)
        {
            signalBus.Subscribe<SelectStorySignal>(x => SelectStoryEventHandler(x.storyId));
        }

        private void SelectStoryEventHandler(string storyId)
        {

        }
    }
}