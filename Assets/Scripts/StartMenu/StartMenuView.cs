using UnityEngine;
using UnityEngine.UI;

namespace StartMenu
{
    public class StartMenuView : MonoBehaviour
    {
        [SerializeField] private GameObject startPanel, storySelectorPanel;
        [SerializeField] private Button openStorySelectorPanelBtn, returnToStartPanelBtn;

        private void Awake()
        {
            BindUI();
        }

        private void BindUI()
        {
            openStorySelectorPanelBtn.onClick.AddListener(() =>
            {
                startPanel.SetActive(false);
                storySelectorPanel.SetActive(true);
            });

            returnToStartPanelBtn.onClick.AddListener(() =>
            {
                storySelectorPanel.SetActive(false);
                startPanel.SetActive(true);
            });
        }
    }
}