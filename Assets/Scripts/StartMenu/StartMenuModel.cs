﻿using UnityEngine;

namespace StartMenu
{
    class StartMenuModel
    {
        private StartMenuView view;
        private SelectorElementsFactory factory;
        private SelectorElement[] elements;

        public StartMenuModel(StartMenuView view, SelectorElementsFactory elementsFactory)
        {
            this.view = view;
            factory = elementsFactory;

            FillStorySelector();
        }

        private void FillStorySelector()
        {
            var storiesData = Resources.LoadAll<DialogueContainer>("ProductionStories/");
            DialogueContainer tmpData;
            SelectorElement tmpElem;

            elements = new SelectorElement[storiesData.Length];

            for (int i = 0; i < storiesData.Length; i++)
            {
                tmpData = storiesData[i];
                tmpElem = factory.GetElementInstance();
                tmpElem.Initialize(tmpData.StoryId, tmpData.StoryTitle);

                elements[i] = tmpElem;
            }
        }
    }
}