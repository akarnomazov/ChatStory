using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class DialogueMessage : MonoBehaviour
{
    [SerializeField] private TMP_Text actorNameField, mainTextField;
    [SerializeField] private Image backImg;

    public void Initialize(string mainText, Vector3 colorHSV, string actorName)
    {
        actorNameField.text = actorName;
        mainTextField.text = mainText;

        backImg.color = Color.HSVToRGB(colorHSV.x, colorHSV.y, colorHSV.z);
        colorHSV.z -= 0.4f;
        actorNameField.color = Color.HSVToRGB(colorHSV.x, colorHSV.y, colorHSV.z);
    }

    public class Factory : PlaceholderFactory<DialogueMessage> { }
}
