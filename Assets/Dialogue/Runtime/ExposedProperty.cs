using UnityEngine;

[System.Serializable]
public class ExposedProperty
{
    public string Id;
    public string ActorName = "Actor name";
    public Color ActorColor = Color.white;
}
