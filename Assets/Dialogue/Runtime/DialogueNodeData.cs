using System;
using UnityEngine;

[Serializable]
public class DialogueNodeData
{
    public string Guid;
    public string DialogueText;
    public Vector2 Position;
    public string ActorName;
    public bool twoInput;
}
