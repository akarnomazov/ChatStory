using UnityEngine;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using System.Linq;

public enum Actors { A, B, C, D, E, F, G, H, I, J }

public class DialogueGraph : EditorWindow
{
    private readonly int actorsCount = 10;
    private DialogueGraphView graphView;
    private string fileName = "New narrative";

    [MenuItem("Graph/DialogueGraph")]
    public static void OpenDialogueWindow()
    {
        var window = GetWindow<DialogueGraph>();
        window.titleContent = new GUIContent("Dialogue Graph");
    }

    private void OnEnable()
    {
        ConstructGraphView();
        GenerateToolbar();
        GenerateMiniMap();
        GenerateBlackBoard();
    }

    private void GenerateBlackBoard()
    {
        var blackboard = new Blackboard(graphView);
        blackboard.Add(new BlackboardSection { title = "Exposed properties"});
        //blackboard.addItemRequested = _blackboard => { graphView.AddPropertyToBlacboard(new ExposedProperty() ); };

        blackboard.editTextRequested = (_blacboard, element, newValue) =>
        {
            var oldPropertyName = ((BlackboardField)element).text;
            if (graphView.ExposedProperties.Any(x => x.ActorName == newValue))
            {
                EditorUtility.DisplayDialog
                    ("Error", "This property name already exist, please chise another one", "ok");
                return;
            }

            var propertyIndex = graphView.ExposedProperties.FindIndex(x => x.ActorName == oldPropertyName);
            graphView.ExposedProperties[propertyIndex].ActorName = newValue;
            ((BlackboardField)element).text = newValue;
        };
        
        blackboard.SetPosition(new Rect(10, 30, 250, 300));
        graphView.Add(blackboard);
        graphView.Blackboard = blackboard;

        for (int i = 0; i < actorsCount; i++)
            graphView.AddPropertyToBlackboard(new ExposedProperty() { Id = ((Actors)i).ToString() });
    }

    private void GenerateMiniMap()
    {
        var minimap = new MiniMap { anchored = true };
        var cords = graphView.contentViewContainer.WorldToLocal(new Vector2(maxSize.x - 10, 30));
        minimap.SetPosition(new Rect(cords.x, cords.y, 200, 140));
        graphView.Add(minimap);
    }

    private void ConstructGraphView()
    {
        graphView = new DialogueGraphView(this)
        {
            name = "Dialogue Graph"
        };

        graphView.StretchToParentSize();
        rootVisualElement.Add(graphView);
    }

    private void GenerateToolbar()
    {
        var toolbar = new Toolbar();

        var fileNameTextField = new TextField("File name");
        fileNameTextField.SetValueWithoutNotify(fileName);
        fileNameTextField.MarkDirtyRepaint();
        fileNameTextField.RegisterValueChangedCallback(evt => fileName = evt.newValue);
        toolbar.Add(fileNameTextField);

        toolbar.Add(new Button(() => RequestDataOperation(true)) { text = "Save data" });
        toolbar.Add(new Button(() => RequestDataOperation(false)) { text = "Load data" });

        rootVisualElement.Add(toolbar);
    }

    private void RequestDataOperation(bool save)
    {
        if (string.IsNullOrEmpty(fileName))
            EditorUtility.DisplayDialog("Invalid file name!", "Please enter a valid file name", "ok");

        var saveUtility = GraphSaveUtility.GetInstance(graphView);
        if (save) saveUtility.SaveGraph(fileName);
        else saveUtility.LoadGraph(fileName);
    }

    private void OnDisable()
    {
        rootVisualElement.Remove(graphView);
    }
}
