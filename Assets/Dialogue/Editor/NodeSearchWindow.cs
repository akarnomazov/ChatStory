using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

public class NodeSearchWindow : ScriptableObject, ISearchWindowProvider
{
    private DialogueGraphView graphView;
    private EditorWindow window;
    private Texture2D identationIcon;

    public void Init(EditorWindow window, DialogueGraphView graphView)
    {
        this.graphView = graphView;
        this.window = window;

        identationIcon = new Texture2D(1, 1);
        identationIcon.SetPixel(0, 0, new Color(0, 0, 0, 0));
        identationIcon.Apply();
    }

    public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
    {
        var tree = new List<SearchTreeEntry>
        {
            new SearchTreeGroupEntry(new GUIContent("Create Elements"), 0),
            new SearchTreeGroupEntry(new GUIContent("Dialogue"), 1),
            new SearchTreeEntry(new GUIContent("Dialogue Node", identationIcon))
            {
                userData = "DefaultNode", level = 2,
            },
            new SearchTreeEntry(new GUIContent("Two Input Node", identationIcon))
            {
                userData = "TwoInputNode", level = 2,
            }
        };
        return tree;
    }

    public bool OnSelectEntry(SearchTreeEntry SearchTreeEntry, SearchWindowContext context)
    {
        var worldMousePosition = window.rootVisualElement.ChangeCoordinatesTo(
            window.rootVisualElement.parent, context.screenMousePosition - window.position.position);
        var localMousePosition = graphView.contentViewContainer.WorldToLocal(worldMousePosition);

        switch (SearchTreeEntry.userData)
        {
            case "DefaultNode":
                graphView.CreateNode("Dialogue Node", localMousePosition);
                return true;
            case "TwoInputNode":
                graphView.CreateTwoInputNode("Dialogue Node", localMousePosition);
                return true;
            default:
                Debug.Log("work");
                return false;
        }
    }
}
