using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

public class GraphSaveUtility
{
    private DialogueGraphView targetGraphView;
    private DialogueContainer containerCach;

    private List<Edge> edges => targetGraphView.edges.ToList();
    private List<DialogueNode> nodes => targetGraphView.nodes.ToList().Cast<DialogueNode>().ToList();

    public static GraphSaveUtility GetInstance(DialogueGraphView _targetGraphView)
    {
        return new GraphSaveUtility
        {
            targetGraphView = _targetGraphView
        };
    }

    public void SaveGraph(string fileName) 
    {
        var dialogueContainer = ScriptableObject.CreateInstance<DialogueContainer>();
        SaveExposedProperties(dialogueContainer);
        SaveMainInfo(dialogueContainer);
        if (!SaveNodes(dialogueContainer)) return;
        if (!AssetDatabase.IsValidFolder("Assets/Resources/InWorkStories"))
            AssetDatabase.CreateFolder("Assets", "Resources/InWorkStories");

        AssetDatabase.CreateAsset(dialogueContainer, $"Assets/Resources/InWorkStories/{fileName}.asset");
        AssetDatabase.SaveAssets();
    }

    private void SaveMainInfo(DialogueContainer dialogueContainer)
    {
        dialogueContainer.StoryTitle = targetGraphView.StoryTitle;
        dialogueContainer.StoryId = targetGraphView.StoryId;
    }

    private void SaveExposedProperties(DialogueContainer dialogueContainer)
    {
        dialogueContainer.ExposedProperties.AddRange(targetGraphView.ExposedProperties);
    }

    private bool SaveNodes(DialogueContainer dialogueContainer)
    {
        if (!edges.Any()) return false;
        
        var connectedPorts = edges.Where(x => x.input.node != null).ToArray();
        for (int i = 0; i < connectedPorts.Length; i++)
        {
            var outputNode = connectedPorts[i].output.node as DialogueNode;
            var inputNode = connectedPorts[i].input.node as DialogueNode;
            
            dialogueContainer.NodeLinks.Add(new NodeLinkData
            {
                BaseNodeGuid = outputNode.GUID,
                PortName = connectedPorts[i].output.portName,
                TargetNodeGuid = inputNode.GUID
                            });
        }

        foreach (var dialogueNode in nodes.Where(node => !node.EntryPoint))
        {
            var name = dialogueContainer.ExposedProperties.First(p =>
                dialogueNode.ActorId.ToString() == p.Id).ActorName;
            dialogueContainer.DialogueNodeData.Add(new DialogueNodeData
            {
                Guid = dialogueNode.GUID,
                DialogueText = dialogueNode.DialogueText,
                ActorName = name,
                Position = dialogueNode.GetPosition().position,
                twoInput = edges
                    .Where(x => (x.input.node as DialogueNode).GUID == dialogueNode.GUID)
                    .Count() > 1
            });
        }

        return true;
    }

    public void LoadGraph(string fileName) 
    {
        containerCach = Resources.Load<DialogueContainer>($"InWorkStories/{fileName}");
        if (containerCach == null)
        {
            EditorUtility.DisplayDialog("File not foung", "Target dialogue graph file does not exists", "ok");
            return;
        }

        CatchMainInfo();
        ClearGraph();
        CreateExposedProperties();
        CreateNodes();
        ConnectNodes();
    }

    private void CatchMainInfo() =>
        targetGraphView.SetMainInfo(containerCach.StoryTitle, containerCach.StoryId);

    private void CreateExposedProperties()
    {
        //targetGraphView.ClearBlackboardAndExposedProprties();

        foreach (var exposedProperty in containerCach.ExposedProperties)
            targetGraphView.AddPropertyInfo(exposedProperty);
    }

    private void CreateNodes()
    {
        foreach (var nodeData in containerCach.DialogueNodeData)
        {
            var tempNode = targetGraphView.CreateDialogueNode(nodeData.ActorName, Vector2.zero, nodeData.DialogueText, nodeData.twoInput);
            tempNode.GUID = nodeData.Guid;
            targetGraphView.AddElement(tempNode);

            var nodePorts = containerCach.NodeLinks.Where(x => x.BaseNodeGuid == nodeData.Guid).ToList();
            nodePorts.ForEach(x => targetGraphView.AddChoicePort(tempNode, x.PortName));
        }
    }

    private void ClearGraph()
    {
        nodes.Find(x => x.EntryPoint).GUID = containerCach.NodeLinks[0].BaseNodeGuid;

        foreach (var node in nodes)
        {
            if (node.EntryPoint) continue;
            edges.Where(x => x.input.node == node).ToList()
                .ForEach(edge => targetGraphView.RemoveElement(edge));

            targetGraphView.RemoveElement(node);
        }
    }

    private void ConnectNodes()
    {
        for (int i = 0; i < nodes.Count; i++)
        {
            var connections = containerCach.NodeLinks.Where(x => x.BaseNodeGuid == nodes[i].GUID).ToList();
            for (int j = 0; j < connections.Count; j++)
            {
                var targetNodeGuid = connections[j].TargetNodeGuid;
                var targetNode = nodes.First(x => x.GUID == targetNodeGuid);
                var inputPort = (Port)targetNode.inputContainer[0];
                if (((Port)targetNode.inputContainer[0]).connected)
                    inputPort = (Port)targetNode.inputContainer[1];

                LinkNodes(nodes[i].outputContainer[j].Q<Port>(), inputPort);
                
                targetNode.SetPosition(new Rect(
                    containerCach.DialogueNodeData.First(x => x.Guid == targetNodeGuid).Position,
                    targetGraphView.DefaultNodeSize
                ));
            }
        }
    }

    private void LinkNodes(Port output, Port input)
    {
        var tempEdge = new Edge
        {
            output = output,
            input = input
        };

        tempEdge?.input.Connect(tempEdge);
        tempEdge?.output.Connect(tempEdge);
        targetGraphView.Add(tempEdge);
    }
}

