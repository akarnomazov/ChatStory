using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class DialogueGraphView : GraphView
{
    public readonly Vector2 DefaultNodeSize = new Vector2(150, 500);
    public Blackboard Blackboard;
    public List<ExposedProperty> ExposedProperties = new List<ExposedProperty>();
    private List<TextField> propertyNames = new List<TextField>();
    private List<ColorField> propertyColors = new List<ColorField>();
    private NodeSearchWindow searchWindow;
    public string StoryTitle { get; private set; }
    public string StoryId { get; private set; }

    public DialogueGraphView(EditorWindow editorWindow)
    {
        styleSheets.Add(Resources.Load<StyleSheet>("DialogueGraph"));
        SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);

        this.AddManipulator(new ContentDragger());
        this.AddManipulator(new SelectionDragger());
        this.AddManipulator(new RectangleSelector());

        var grid = new GridBackground();
        Insert(0, grid);
        grid.StretchToParentSize();

        AddElement(GenerateEntryPointNode());
        AddSearchWindow(editorWindow);
    }

    public void SetMainInfo(string storyTitle, string storyId)
    {
        StoryTitle = storyTitle;
        StoryId = storyId;
    }

    private void AddSearchWindow(EditorWindow editorWindow)
    {
        searchWindow = ScriptableObject.CreateInstance<NodeSearchWindow>();
        searchWindow.Init(editorWindow, this);
        nodeCreationRequest = context => 
            SearchWindow.Open(new SearchWindowContext(context.screenMousePosition), searchWindow);
    }

    public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
    {
        var compatiblePorts = new List<Port>();
        ports.ForEach((port) =>
        {
            if (startPort != port && startPort.node != port.node)
                compatiblePorts.Add(port);
        });

        return compatiblePorts;
    }

    private Port GeneratePort(DialogueNode node, Direction portDirection, Port.Capacity capacity = Port.Capacity.Single)
    {
        return node.InstantiatePort(Orientation.Horizontal, portDirection, capacity, typeof(float));
    }

    private DialogueNode GenerateEntryPointNode()
    {
        var node = new DialogueNode
        {
            title = "Start",
            GUID = Guid.NewGuid().ToString(),
            DialogueText = "EntryPoint",
            EntryPoint = true
        };

        var generatedPort = GeneratePort(node, Direction.Output);
        generatedPort.portName = "Next";
        node.outputContainer.Add(generatedPort);

        node.capabilities &= ~Capabilities.Movable;
        node.capabilities &= ~Capabilities.Deletable;

        node.RefreshExpandedState();
        node.RefreshPorts();

        node.SetPosition(new Rect(300, 200, 100, 150));
        return node;
    }

    public void CreateNode(string actorName, Vector2 position, string dialogueText = "")
    {
        AddElement(CreateDialogueNode(actorName, position, dialogueText));
    }

    public void CreateTwoInputNode(string actorName, Vector2 position, string dialogueText = "")
    {
        AddElement(CreateDialogueNode(actorName, position, dialogueText, true));
    }

    public DialogueNode CreateDialogueNode(string actorName, Vector2 position, string dialogueText = "", bool twoInputNode = false)
    {
        var dialogueNode = new DialogueNode
        {
            title = actorName,
            DialogueText = actorName,
            GUID = Guid.NewGuid().ToString()
        };

        var inputPort = GeneratePort(dialogueNode, Direction.Input, Port.Capacity.Multi);
        inputPort.portName = "Input";
        dialogueNode.inputContainer.Add(inputPort);

        if (twoInputNode)
        {
            var secondInputPort = GeneratePort(dialogueNode, Direction.Input, Port.Capacity.Multi);
            secondInputPort.portName = "Input2";
            dialogueNode.inputContainer.Add(secondInputPort);
        }

        dialogueNode.styleSheets.Add(Resources.Load<StyleSheet>("Node"));

        var button = new Button(() => AddChoicePort(dialogueNode)) { text = "New Choice" };
        dialogueNode.titleButtonContainer.Add(button);

        var actorSelector = new EnumField(new Actors());
        actorSelector.RegisterValueChangedCallback(evt =>
        {
            dialogueNode.ActorId = (Actors)evt.newValue;
        });
        
        Actors tmpSelected = Actors.A;
        try
        {
            Enum.TryParse(ExposedProperties.Find(p => p.ActorName == actorName).Id, out tmpSelected);
        }
        catch { }
        actorSelector.SetValueWithoutNotify(tmpSelected);
        dialogueNode.ActorId = tmpSelected;
        dialogueNode.mainContainer.Add(actorSelector);

        var textField = new TextField(string.Empty);
        textField.RegisterValueChangedCallback(evt => 
        {
            dialogueNode.DialogueText = evt.newValue;
        });
        textField.multiline = true;
        textField.styleSheets.Add(Resources.Load<StyleSheet>("TextFieldStyle"));
        var textTmp = (string.IsNullOrEmpty(dialogueText)) ? "Main text" : dialogueText;
        textField.SetValueWithoutNotify(textTmp);
        dialogueNode.DialogueText = textTmp;
        dialogueNode.mainContainer.Add(textField);

        dialogueNode.SetPosition(new Rect(position, DefaultNodeSize));
        dialogueNode.RefreshExpandedState();
        dialogueNode.RefreshPorts();
        return dialogueNode;
    }

    public void AddChoicePort(DialogueNode dialogueNode, string overridePortName = "")
    {
        var generatedPort = GeneratePort(dialogueNode, Direction.Output);

        var oldLabel = generatedPort.contentContainer.Q<Label>("type");
        generatedPort.contentContainer.Remove(oldLabel);

        var outputPortCount = dialogueNode.outputContainer.Query("connector").ToList().Count;

        var choicePortName = string.IsNullOrEmpty(overridePortName) 
            ? "null" 
            : overridePortName;

        var textField = new TextField
        {
            name = string.Empty,
            value = choicePortName
        };
        generatedPort.portName = choicePortName;

        textField.RegisterValueChangedCallback(evt => generatedPort.portName = evt.newValue);
        generatedPort.contentContainer.Add(new Label(" "));
        generatedPort.contentContainer.Add(textField);
        var deleteBtn = new Button(() => RemovePort(dialogueNode, generatedPort)) { text = "X" };
        generatedPort.contentContainer.Add(deleteBtn);

        generatedPort.name = choicePortName;
        dialogueNode.outputContainer.Add(generatedPort);
        dialogueNode.RefreshPorts();
        dialogueNode.RefreshExpandedState();
    }

    private void RemovePort(DialogueNode dialogueNode, Port generatedPort)
    {
        var targetEdges = edges.ToList().Where(x => x.output.node == generatedPort.node);

        if (targetEdges.Any())
        {
            var edge = targetEdges.First();
            edge.input.Disconnect(edge);
            RemoveElement(targetEdges.First());
        }

        dialogueNode.outputContainer.Remove(generatedPort);
        dialogueNode.RefreshPorts();
        dialogueNode.RefreshExpandedState();
    }

    public void ClearBlackboardAndExposedProprties()
    {
        ExposedProperties.Clear();
        Blackboard.Clear();
    }

    public void AddPropertyToBlackboard(ExposedProperty exposedProperty)
    {
        ExposedProperty property;
        var tmpActorName = (exposedProperty.Id == "J") ? "neutral" : "simple actor";

        property = new ExposedProperty()
        {
            Id = exposedProperty.Id,
            ActorName = tmpActorName,
            ActorColor = Color.white
        };
        ExposedProperties.Add(property);

        var container = new VisualElement();
        var propertyTitle = new BlackboardField
        {
            text = property.Id,
            typeText = "Actor id"
        };
        container.Add(propertyTitle);

        var fieldsContainer = new VisualElement();
        var propertyActorNameField = new TextField("Name")
        {
            value = tmpActorName,
        };
        propertyActorNameField.RegisterValueChangedCallback(evt =>
        {
            var changePropertyIndex = ExposedProperties.FindIndex(x => x.Id == property.Id);
            ExposedProperties[changePropertyIndex].ActorName = evt.newValue;
        });
        fieldsContainer.Add(propertyActorNameField);
        propertyNames.Add(propertyActorNameField);

        var propertyValueColorField = new ColorField("Color")
        {
            value = exposedProperty.ActorColor
        };
        propertyValueColorField.RegisterValueChangedCallback(evt =>
        {
            var changePropertyIndex = ExposedProperties.FindIndex(x => x.Id == property.Id);
            ExposedProperties[changePropertyIndex].ActorColor = evt.newValue;
        });
        fieldsContainer.Add(propertyValueColorField);
        propertyColors.Add(propertyValueColorField);

        var blackBoardValueRow1 = new BlackboardRow(propertyTitle, fieldsContainer);
        container.Add(blackBoardValueRow1);

        Blackboard.Add(container);
    }

    public void AddPropertyInfo(ExposedProperty exposedProperty)
    {
        var index = ExposedProperties.FindIndex(p => p.Id == exposedProperty.Id);
        ExposedProperties[index] = exposedProperty;
        propertyNames[index].value = exposedProperty.ActorName;
        propertyColors[index].value = exposedProperty.ActorColor;
    }
}
